package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    //functionality for creating post
    void createPost(String stringToken, Post post);

    Iterable<Post> getPosts();

    // Updating post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete post
    ResponseEntity deletePost(Long id, String stringToken);

}